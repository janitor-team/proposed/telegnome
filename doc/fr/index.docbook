<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY version "0.1.0">
]>
<article id="telegnome" lang="fr">
  <articleinfo>
    <title>Guide utilisateur de TeleGNOME</title>
    <abstract role="description">
      <para>TeleGNOME télécharge et affiche des pages Teletext depuis l'Internet</para>
    </abstract>
    <authorgroup>
      <author>
	<firstname>Arjan</firstname> <surname>Scherpenisse</surname>
	<affiliation>
	  <address>
	    <email>acscherp@wins.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Dirk-Jan</firstname> <surname>Binnema</surname>
	<affiliation>
	  <address>
	    <email>djcb@dds.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Ork</firstname> <surname>de Rooij</surname>
	<affiliation>
	  <address>
	    <email>ork.derooij@student.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author role="maintainer">
	<firstname>Colin</firstname> <surname>Watson</surname>
	<affiliation>
	  <address>
	    <email>cjwatson@debian.org</email>
	  </address>
	</affiliation>
      </author>
    </authorgroup>
    <copyright>
      <year>1999, 2000</year> <holder>Arjan Scherpenisse</holder>
    </copyright>
    <copyright>
      <year>2008</year> <holder>Colin Watson</holder>
    </copyright>
    <legalnotice>
      <para>Cette documentation est libre ; vous pouvez la redistribuer et/ou la modifier selon les termes de la licence GNU GPL telle qu'elle est publiée par la Free Software Foundation; que ce soit la version 2 de la licence ou (à votre convenance) n'importe quelle version suivante.</para>
      
      <para>Ce programme est distribué dans l'espoir qu'il soit utile, mais IL N'Y A AUCUNE GARANTIE; sans même la garantie implicite de COMMERCIALISATION ou D'ADAPTATION À UN USAGE PARTICULIER. Voir la GNU GPL pour plus de détails.</para>
      
      <para>You should have received a copy of the GNU General Public
	License along with this program; if not, see
	<ulink type="http" url="http://www.gnu.org/licenses/"/></para>
      
      <para>Pour plus de détails, voir le fichier COPYING dans la distribution source de GNOME.</para>
    </legalnotice>
    <revhistory>
      <revision>
	<revnumber>TeleGNOME User's Guide 0.1.0</revnumber>
	<date>2008-04-27</date>
      </revision>
    </revhistory>
  </articleinfo>
 
  <sect1 id="intro">
    <title>Introduction</title>
    <para>À faire.</para>
  </sect1>    
  
  <sect1 id="ui">
    <title>Interface utilisateur</title> 
    
    <para>L'interface principale est constituée d'une barre de menu, d'une barre d'outils, de la page actuellement visionnée et de la barre de statut.</para>
    
    <sect2>
      <title>Le menu</title> 

      <para>La barre de menu est constituée de 3 menus déroulants, appelés « Fichier », « Paramètres » et « Aide ». Les options suivantes sont disponibles dans chacun de ces menus.</para>
      
      <itemizedlist>
	<listitem>
	  <para>Fichier</para>
	</listitem>
	<listitem>
	  <para>Paramètres</para>
	</listitem>
	<listitem>
	  <para>Canaux</para>
	</listitem>
	<listitem>
	  <para>Aide</para>
	</listitem>
      </itemizedlist>
    </sect2>
  </sect1>

  <sect1 id="using">
    
    <title>Utilisation de TeleGNOME</title>
    
    <para>Un télétexte classique consiste en une série de pages qui sont transmises rapidement et successivement par le diffuseur. À chaque fois que vous cherchez à régler votre téléviseur sur une page spécifique, votre téléviseur capte uniquement cette page et la montre à l'écran.</para>
  
    <para>Toutes les pages peuvent avoir des sous-pages, ce qui signifie que des sous-pages sont accouplées à la page. De cette façon, le nombre de pages peut rester le même mais la quantité d'information peut varier. Par exemple : la page donnant les nouvelles (101 sur Dutch TV) peut n'avoir que peu d'éléments d'information (rien ne se passe dans le monde), et ainsi ne pas avoir de sous-pages, ou alors elle peut avoir beaucoup d'éléments d'information, répartis sur plusieurs sous-pages. Ces sous-pages sont habituellement mises à jour automatiquement sur votre téléviseur, persistant suffisamment longtemps pour que page puisse être lue. Cela signifie que si vous voulez voir la 7ème sous page, et que la page affichée actuellement est la 3ème, vous devrez attendre un peu. Cela peut être plutôt ennuyeux. Cependant, si le minutage est trop rapide, la page risque de disparaître avant que vous ne l'ayez lue, et il vous faudra attendre que la page revienne.</para>
  
    <para>Utiliser <application>TeleGNOME</application> est similaire à utiliser du télétexte sur un téléviseur classique, mais sans l'attente, qui peut être pénible, de l'apparition d'une page.</para>

    <para>Comme pour le télétexte sur un téléviseur, vous devez simplement saisir le numéro de la page et appuyer sur <keycap>ENTRÉE</keycap> pour charger la page. Ce numéro peut être n'importe quel numéro servi par le canal de télétexte actuel, et il n'est pas restreint aux numéros affichés sur la page actuelle.</para>

    <para>Vous pouvez charger les sous-pages directement en saisissant un numéro de page, un tiret ou une barre oblique (slash), et le numéro de sous-page. Par exemple : si vous voulez charger la deuxième page de la page 201, saisissez simplement <keycap>201/2</keycap>.</para>

    <para>Quand vous êtes prêt à lire la sous-page suivante (si elle existe), vous devez appuyer sur le bouton <keycap>Suivant</keycap>. La page suivante est alors chargée. Vous pouvez aussi saisir directement le numéro de la sous-page voulue en utilisant le clavier.</para>
  </sect1>
</article>
