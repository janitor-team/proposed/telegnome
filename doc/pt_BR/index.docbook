<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd" [
<!ENTITY version "0.1.0">
]>
<article id="telegnome" lang="pt-BR">
  <articleinfo>
    <title>Ajuda do TeleGNOME</title>
    <abstract role="description">
      <para>O TeleGNOME baixa e exibe páginas de teletexto da internet.</para>
    </abstract>
    <authorgroup>
      <author>
	<firstname>Arjan</firstname> <surname>Scherpenisse</surname>
	<affiliation>
	  <address>
	    <email>acscherp@wins.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Dirk-Jan</firstname> <surname>Binnema</surname>
	<affiliation>
	  <address>
	    <email>djcb@dds.nl</email>
	  </address>
	</affiliation>
      </author>
      <author>
	<firstname>Ork</firstname> <surname>de Rooij</surname>
	<affiliation>
	  <address>
	    <email>ork.derooij@student.uva.nl</email>
	  </address>
	</affiliation>
      </author>
      <author role="maintainer">
	<firstname>Colin</firstname> <surname>Watson</surname>
	<affiliation>
	  <address>
	    <email>cjwatson@debian.org</email>
	  </address>
	</affiliation>
      </author>
    </authorgroup>
    <copyright>
      <year>1999, 2000</year> <holder>Arjan Scherpenisse</holder>
    </copyright>
    <copyright>
      <year>2008</year> <holder>Colin Watson</holder>
    </copyright>
    <legalnotice>
      <para>Esta documentação é um software livre; você pode redistribuí-lo e/ou modificá-lo sob os termos da Licença Pública Geral GNU publicada pela Free Software Foundation; qualquer versão 2 da Licença, ou (a seu critério) outra versão posterior.</para>
      
      <para>Este programa é distribuído na expectativa de que seja útil, mas SEM NENHUMA GARANTIA; sem mesmo implicar garantias de COMERCIALIZAÇÃO ou ADEQUAÇÃO A UM FIM ESPECÍFICO. Veja a Licença Pública Geral GNU (GPL) para mais detalhes.</para>
      
      <para>You should have received a copy of the GNU General Public
	License along with this program; if not, see
	<ulink type="http" url="http://www.gnu.org/licenses/"/></para>
      
      <para>Para mais detalhes consulte o arquivo COPYING-DOCS distribuído com este manual.</para>
    </legalnotice>
    <revhistory>
      <revision>
	<revnumber>TeleGNOME User's Guide 0.1.0</revnumber>
	<date>2008-04-27</date>
      </revision>
    </revhistory>
  </articleinfo>
 
  <sect1 id="intro">
    <title>Introdução</title>
    <para>TBD.</para>
  </sect1>    
  
  <sect1 id="ui">
    <title>A interface gráfica</title> 
    
    <para>A interface principal consiste de uma barra de menu, uma barra de ferramentas, a página visualizada atualmente e a barra de status.</para>
    
    <sect2>
      <title>O menu</title> 

      <para>A barra de menu consiste de 3 menus suspensos, chamados "Arquivo", "Configurações", e "Ajuda". As seguintes opções estão disponíveis em cada um destes menus.</para>
      
      <itemizedlist>
	<listitem>
	  <para>Arquivo</para>
	</listitem>
	<listitem>
	  <para>Configurações</para>
	</listitem>
	<listitem>
	  <para>Canais</para>
	</listitem>
	<listitem>
	  <para>Ajuda</para>
	</listitem>
      </itemizedlist>
    </sect2>
  </sect1>

  <sect1 id="using">
    
    <title>Usando o TeleGNOME</title>
    
    <para>O Teletexto normal consiste de uma série de páginas, as quais são transmitidas em sucessão rápida pela transmissora. Toda vez que você desejar que sua TV sintonize em uma página específica, sua TV irá capturar apenas aquela página e a mostrará na tela.</para>
  
    <para>Cada página pode ter subpáginas, ou seja: a página consiste de várias subpáginas. Desta forma, as numerações das páginas podem permanecerem as mesmas, porém a quantidade de informações em uma página pode variar. Por exemplo: a página que distribui a notícia (101 na TV holandesa) pode possuir apenas alguns itens de informação (nada aconteceu no mundo) e, portanto, não há subpáginas ou isto pode ter muito itens, espalhados em subpáginas. Geralmente estas subpáginas são automaticamente atualizadas na sua TV, temporizadas por um intervalo no qual a página possa ser lida. Isto significa que se você quiser ver a subpágina 7 e a atual subpágina em exibição é a 3, você terá que esperar por algum tempo. Isso pode ser muito chato. Ademais, se o tempo é muito rápido, a página pode ter desaparecido antes que você a tenha lido, o que significa que você terá que aguardar a página aparecer novamente.</para>
  
    <para>Usar o <application>TeleGNOME</application> é bem similar a usar teletexto em um aparelho de televisão normal, porém sem os irritantes longos tempos de espera que, de vez em quando, ocorrem para a página aparecer.</para>

    <para>Assim como o teletexto na TV, você pode simplesmente digitar o número da página e pressionar <keycap>ENTER</keycap> para carregar tal página. Este número pode ser qualquer um que possa ser atendido pelo canal atual de teletexto, e isto não é restrito aos números mostrados na página atual.</para>

    <para>Você pode diretamente carregar subpáginas ao digitar o número da página, um traço ou uma barra e o número da subpágina. Por exemplo: se você deseja carregar a segunda página da página 201, basta digitar <keycap>201/2</keycap>.</para>

    <para>Quando você está pronto para ler a próxima subpágina (caso exista), você pode pressionar o botão <keycap>próxima</keycap>. Isto irá carregar a próxima subpágina. De forma alternativa, você pode simplesmente digitar a subpágina solicitada imediatamente usando o teclado.</para>
  </sect1>
</article>
